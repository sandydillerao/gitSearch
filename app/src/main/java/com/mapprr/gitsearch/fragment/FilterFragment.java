package com.mapprr.gitsearch.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.allattentionhere.fabulousfilter.AAH_FabulousFragment;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.mapprr.gitsearch.MyApplication;
import com.mapprr.gitsearch.R;
import com.mapprr.gitsearch.model.Filter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by sandy on 07-11-2017.
 */

public class FilterFragment extends AAH_FabulousFragment {
    @BindView(R.id.date_filter_layout)
    CardView cardView;

    @BindView(R.id.txtFromDate)
    TextView txtFromDate;

    @BindView(R.id.txtToDate)
    TextView txtToDate;
    @BindView(R.id.imgbtn_clear)
    ImageButton imgbtnClear;
    @BindView(R.id.imgbtn_apply)
    ImageButton imgbtnApply;
    Unbinder unbinder;
    @BindView(R.id.rgSortby)
    RadioGroup rgSortby;
    @BindView(R.id.rgOrderBy)
    RadioGroup rgOrderBy;
    Unbinder unbinder1;

    private Filter filter_apply = new Filter();

    Context context;

    public static FilterFragment getInstance() {
        FilterFragment filter = new FilterFragment();
        return filter;
    }

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        final View contentView = View.inflate(getContext(), R.layout.dialog_filter, null);
        context = contentView.getContext();
        unbinder = ButterKnife.bind(this, contentView);
        RelativeLayout rl_content = (RelativeLayout) contentView.findViewById(R.id.rl_content);
        LinearLayout ll_buttons = (LinearLayout) contentView.findViewById(R.id.ll_buttons);

        contentView.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFilter("closed");
            }
        });

        /*set filters*/
        final Filter oldFilter = MyApplication.getInstance().getFilter();
        if (oldFilter != null) {
            txtFromDate.setText(oldFilter.getCreatedFrom()!=null ? oldFilter.getCreatedFrom() : "DD/MM/YYYY");
            txtToDate.setText(oldFilter.getCreatedTo()!=null ? oldFilter.getCreatedTo() : "DD/MM/YYYY");

            /*set selected sortBy from prev filter*/
            for (int i=0;i<rgSortby.getChildCount();i++) {
                View radioButton = rgSortby.getChildAt(i);
                if (radioButton instanceof RadioButton) {
                    RadioButton rb =(RadioButton)radioButton;
                    if((rb.getText().toString().toLowerCase()).equals(oldFilter.getSortBy())) {
                        rb.setChecked(true);
                    }
                }
            }

            /*set selected orderBy from prev filter*/
            for (int i=0;i<rgOrderBy.getChildCount();i++) {
                View orderByrb = rgOrderBy.getChildAt(i);
                if (orderByrb instanceof RadioButton) {
                    RadioButton rb =(RadioButton)orderByrb;
                    if((rb.getText().toString().toLowerCase()).equals(oldFilter.getOrderBy())) {
                        rb.setChecked(true);
                    }
                }
            }

        }

        /*open date selector dialog*/
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dpd = null;
                if(oldFilter!=null && oldFilter.getCreatedFrom()!=null){
                    Calendar fromDate = Calendar.getInstance();
                    fromDate.setTime(toDate(oldFilter.getCreatedFrom()));
                    Calendar toDate = Calendar.getInstance();
                    toDate.setTime(toDate(oldFilter.getCreatedTo()));

                    dpd= DatePickerDialog.newInstance(
                            datetimeCallback(),
                            fromDate.get(Calendar.YEAR),
                            fromDate.get(Calendar.MONTH),
                            fromDate.get(Calendar.DAY_OF_MONTH),
                            toDate.get(Calendar.YEAR),
                            toDate.get(Calendar.MONTH),
                            toDate.get(Calendar.DAY_OF_MONTH)
                    );
                }else{
                    Calendar now = Calendar.getInstance();
                    dpd= DatePickerDialog.newInstance(
                            datetimeCallback(),
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );
                }

                dpd.setAutoHighlight(true);
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        /*Action apply filter*/
        imgbtnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.getInstance().setFilter(filter_apply);
                closeFilter(filter_apply);
            }
        });

        /*Action clear filter*/
        imgbtnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter_apply.clearFilter();
                closeFilter(filter_apply);
                MyApplication.getInstance().setFilter(filter_apply);
            }
        });

        rgSortby.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                String  sortBy= ((RadioButton)contentView.findViewById(id)).getText().toString();
                filter_apply.setSortBy(sortBy.toLowerCase());
            }
        });

        rgOrderBy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                String orderBy = ((RadioButton)contentView.findViewById(id)).getText().toString();
                filter_apply.setOrderBy(orderBy.toLowerCase());
            }
        });

        //params to set
        setAnimationDuration(400); //optional; default 500ms
        setPeekHeight(400); // optional; default 400dp
        setCallbacks((Callbacks) getActivity()); //optional; to get back result
        setViewgroupStatic(ll_buttons); // optional; layout to stick at bottom on slide
        setViewMain(rl_content); //necessary; main bottomsheet view
        setMainContentView(contentView); // necessary; call at end before super
        super.setupDialog(dialog, style); //call super at last
    }

    private DatePickerDialog.OnDateSetListener datetimeCallback() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
                String log = "You picked the following date: From- " + year + "-" + (++monthOfYear) + "-" + dayOfMonth + " To " + dayOfMonthEnd + "/" + (++monthOfYearEnd) + "/" + yearEnd;
                Log.d("DateResult", log);

                String fromDate = toDateFormat(year + "-" + (monthOfYear) + "-" + dayOfMonth);
                String toDate = toDateFormat(yearEnd + "-" + (monthOfYearEnd) + "-" + dayOfMonthEnd);

                filter_apply.setCreatedFrom(fromDate);
                filter_apply.setCreatedTo(toDate);
                txtFromDate.setText(fromDate);
                txtToDate.setText(toDate);
            }
        };
    }

    private String toDateFormat(String date) {
        Date date1 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date1);
    }

    private Date toDate(String dateStr){
        Date date= null;
        try {
            date  = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}

package com.mapprr.gitsearch.activity;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.mapprr.gitsearch.R;
import com.mapprr.gitsearch.adapter.RepoSearchAdapter;
import com.mapprr.gitsearch.api.RestAPI;
import com.mapprr.gitsearch.api.RestClient;
import com.mapprr.gitsearch.model.Contributor;
import com.mapprr.gitsearch.model.Repo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContributorDetailsActivity extends AppCompatActivity {

    @BindView(R.id.imgAvatar)
    CircleImageView imgAvatar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private Contributor mContributor;

    private RecyclerView.LayoutManager linearLayoutManager;
    private ArrayList<Repo> mRepoArrayList;
    private RepoSearchAdapter mRepoSearchAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributor_details);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContributor = (Contributor) getIntent().getSerializableExtra("Contributor");
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        init();
        loadRespositories();
    }

    private void init() {
        Picasso.with(getApplicationContext())
                .load(mContributor.getAvatarUrl())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(imgAvatar);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle(mContributor.getLogin());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadRespositories() {
        RestAPI restAPI = RestClient.getClient().create(RestAPI.class);
        Call<ArrayList<Repo>> call = restAPI.getRepositories(mContributor.getLogin());
        call.enqueue(new Callback<ArrayList<Repo>>() {
            @Override
            public void onResponse(Call<ArrayList<Repo>> call, Response<ArrayList<Repo>> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    mRepoArrayList = response.body();
                    mRepoSearchAdapter = new RepoSearchAdapter(mRepoArrayList, getApplicationContext(), false);
                    recyclerView.setAdapter(mRepoSearchAdapter);
                    recyclerView.setNestedScrollingEnabled(false);
                    mRepoSearchAdapter.notifyDataSetChanged();
                } else {
//                    Toast.makeText(getApplicationContext(), "No Search Result", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Repo>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}

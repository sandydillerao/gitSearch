package com.mapprr.gitsearch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mapprr.gitsearch.R;
import com.mapprr.gitsearch.adapter.GridContributorAdapter;
import com.mapprr.gitsearch.api.RestAPI;
import com.mapprr.gitsearch.api.RestClient;
import com.mapprr.gitsearch.model.Contributor;
import com.mapprr.gitsearch.model.Repo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepoDetailsActivity extends AppCompatActivity {
    @BindView(R.id.imgAvatar)
    ImageView imgAvatar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.projectLink)
    TextView projectLink;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.contributorGrid)
    GridView contributorGrid;
    @BindView(R.id.noContent)
    TextView noContent;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private Repo mRepo;

    private ArrayList<Contributor> mContributors;
    private GridContributorAdapter mGridContributorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_details);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRepo = (Repo) getIntent().getSerializableExtra("Repo");

        Picasso.with(getApplicationContext())
                .load(mRepo.getOwner().getAvatarUrl())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(imgAvatar);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle(mRepo.getName());

        projectLink.setText(Html.fromHtml("<u>" +mRepo.getHtmlUrl()+"</u>"));
        description.setText(mRepo.getDescription());
        loadContributors();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void loadContributors() {
        RestAPI restAPI = RestClient.getClient().create(RestAPI.class);
        Call<ArrayList<Contributor>> call = restAPI.getContributors(mRepo.getOwner().getLogin(), mRepo.getName());
        call.enqueue(new Callback<ArrayList<Contributor>>() {
            @Override
            public void onResponse(Call<ArrayList<Contributor>> call, Response<ArrayList<Contributor>> response) {
                progressBar.setVisibility(View.GONE);
                if (response.code() == 200) {
                    contributorGrid.setVisibility(View.VISIBLE);
                    mContributors = response.body();
                    mGridContributorAdapter = new GridContributorAdapter(mContributors, getApplicationContext());
                    contributorGrid.setAdapter(mGridContributorAdapter);
                    setGridViewHeightBasedOnChildren(contributorGrid, 3);
                } else {
                    noContent.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Contributor>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    @OnClick(R.id.projectLink)
    public void onClick() {
        Intent in = new Intent(this, ProjectWebViewActivity.class);
        in.putExtra("url", mRepo.getHtmlUrl());
        startActivity(in);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = mGridContributorAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if( items > columns ){
            x = items/columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }
}

package com.mapprr.gitsearch.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.allattentionhere.fabulousfilter.AAH_FabulousFragment;
import com.mapprr.gitsearch.R;
import com.mapprr.gitsearch.adapter.RepoSearchAdapter;
import com.mapprr.gitsearch.api.RestAPI;
import com.mapprr.gitsearch.api.RestClient;
import com.mapprr.gitsearch.fragment.FilterFragment;
import com.mapprr.gitsearch.model.Filter;
import com.mapprr.gitsearch.model.Repo;
import com.mapprr.gitsearch.model.SearchResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AAH_FabulousFragment.Callbacks {

    RecyclerView mRecyclerView;
    @BindView(R.id.noResult)
    LinearLayout noResult;
    @BindView(R.id.filter_fab)
    FloatingActionButton filterFab;
    private RepoSearchAdapter mRepoSearchAdapter;
    private ArrayList<Repo> mRepoArrayList;
    private FilterFragment dialogFrag;
    private String queryBy = "sandeep";
    private String orderBy = "desc";
    private String sortBy = "watcher";
    private int perPage = 10;
    private int pageNo = 1;
    private String extraFilter = null;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        loadRepo("sandeep");

        dialogFrag = FilterFragment.getInstance();
        dialogFrag.setParentFab(filterFab);
        filterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogFrag.show(getSupportFragmentManager(), dialogFrag.getTag());
            }
        });

    }


    private void loadRepo(String q) {
        progressDialog.show();
        RestAPI restAPI = RestClient.getClient().create(RestAPI.class);
        Call<SearchResponse> call = restAPI.searchRepository(q, sortBy, orderBy, perPage, pageNo);
        call.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                progressDialog.dismiss();
                if (response.body().getTotalCount() > 0) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    noResult.setVisibility(View.GONE);
                    mRepoArrayList = response.body().getRepos();
                    mRepoSearchAdapter = new RepoSearchAdapter(mRepoArrayList, getApplicationContext(), true);
                    mRecyclerView.setAdapter(mRepoSearchAdapter);
                    mRepoSearchAdapter.notifyDataSetChanged();
                } else {
                    noResult.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "No Search Result", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                progressDialog.hide();
                Log.d("Repo FAILED", t.getMessage().toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem search = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!TextUtils.isEmpty(query) && query.length() >= 2) {
                    if(query!=null)
                    queryBy = query;
                    loadRepo(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return true;
            }
        });
    }


    @Override
    public void onResult(Object result) {
        Log.d("Filter", "onResult: " + result.toString());
        if (result.toString().equalsIgnoreCase("swiped_down") || result.toString().equalsIgnoreCase("closed")) {
            //do something or nothing
        } else {
            //handle filters
            if(result!=null){
                Filter filter_applied = (Filter) result;
                if(filter_applied!=null){
                    sortBy = filter_applied.getSortBy()!=null ? filter_applied.getSortBy() : sortBy;
                    orderBy = filter_applied.getOrderBy()!=null ? filter_applied.getOrderBy() : orderBy;
                    pageNo = filter_applied.getPageNo();
                    perPage = filter_applied.getPerPage();
                    filter_applied.setQueryBy(queryBy);
                    loadRepo(filter_applied.getCustomQuery());

                }
            }
        }
    }
}

package com.mapprr.gitsearch.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mapprr.gitsearch.R;
import com.mapprr.gitsearch.activity.ContributorDetailsActivity;
import com.mapprr.gitsearch.model.Contributor;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by sandy on 05-11-2017.
 */

public class GridContributorAdapter extends BaseAdapter{
    private Context mContext;
    private ArrayList<Contributor> mContributors;
    public GridContributorAdapter(ArrayList<Contributor> mContributors,  Context mContext) {
        this.mContext = mContext;
        this.mContributors = mContributors;
    }

    @Override
    public int getCount() {
        return mContributors.size();
    }

    @Override
    public Object getItem(int i) {
        return mContributors.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mContributors.get(i).getId();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grid_contributor, viewGroup, false);
        ImageView icon = (ImageView) itemView.findViewById(R.id.imgAvatar);
        TextView contributorName = (TextView) itemView.findViewById(R.id.contributorName);
        Picasso.with(mContext)
                .load(mContributors.get(i).getAvatarUrl())
                .placeholder(R.drawable.git_avatar)
                .error(R.drawable.git_avatar)
                .into(icon);
        contributorName.setText(mContributors.get(i).getLogin());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(mContext, ContributorDetailsActivity.class);
                in.putExtra("Contributor", mContributors.get(i));
                mContext.startActivity(in);
            }
        });
        return itemView;
    }
}

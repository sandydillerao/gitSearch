package com.mapprr.gitsearch.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mapprr.gitsearch.R;
import com.mapprr.gitsearch.activity.RepoDetailsActivity;
import com.mapprr.gitsearch.model.Repo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sandy on 04-11-2017.
 */

public class RepoSearchAdapter extends RecyclerView.Adapter<RepoSearchAdapter.DataObjectHolder> {

    private ArrayList<Repo> repoList;
    private Context mContext;
    private boolean mImage;

    public RepoSearchAdapter(ArrayList<Repo> repoList, Context mContext, boolean mImage) {
        this.repoList = repoList;
        this.mContext = mContext;
        this.mImage = mImage;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_repo, parent, false);
        return new DataObjectHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {
        final Repo repo  = repoList.get(position);
        holder.name.setText(repo.getName());
        holder.fullName.setText(repo.getFullName());
        holder.watchCount.setText(String.valueOf(repo.getWatchersCount()));
        holder.starCount.setText(String.valueOf(repo.getStargazersCount()));
        holder.forkCount.setText(String.valueOf(repo.getForksCount()));
        if(mImage){
            Picasso.with(mContext)
                    .load(repo.getOwner().getAvatarUrl())
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .into(holder.imgAvatar);
        }


        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, RepoDetailsActivity.class);
                intent.putExtra("Repo",(Repo) repo);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return repoList.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgAvatar)
        ImageView imgAvatar;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.fullName)
        TextView fullName;
        @BindView(R.id.watchCount)
        TextView watchCount;
        @BindView(R.id.forkCount)
        TextView forkCount;
        @BindView(R.id.starCount)
        TextView starCount;
        @BindView(R.id.card_view)
        CardView card_view;

        public DataObjectHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}

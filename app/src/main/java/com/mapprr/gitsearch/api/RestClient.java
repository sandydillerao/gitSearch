package com.mapprr.gitsearch.api;

import com.mapprr.gitsearch.Config;

import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sandy on 29-07-2017.
 */

public class RestClient {
    private static Retrofit retrofit = null;
    public static Retrofit getClient() {
        if (retrofit==null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

            httpClient.connectTimeout(5, TimeUnit.MINUTES);
            httpClient.readTimeout(5, TimeUnit.MINUTES);
            HttpLoggingInterceptor loggingInterceptor  = new HttpLoggingInterceptor();
            loggingInterceptor .setLevel(HttpLoggingInterceptor.Level.BODY);

            httpClient.addInterceptor(new RestInterceptor());
            httpClient.addInterceptor(loggingInterceptor );  // <-- this is the important line!

            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}

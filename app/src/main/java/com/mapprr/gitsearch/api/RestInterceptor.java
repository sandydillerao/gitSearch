package com.mapprr.gitsearch.api;

import com.mapprr.gitsearch.Config;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by sandy on 03-08-2017.
 */

public class RestInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder requestBuilder = original.newBuilder();

        /*Custom Header*/
        requestBuilder.header("Authorization", "token " + Config.TOKEN);
        requestBuilder.header("Accept", "application/vnd.github.mercy-preview+json");

        return chain.proceed(original);

    }
}

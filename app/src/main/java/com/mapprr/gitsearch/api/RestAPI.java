package com.mapprr.gitsearch.api;


import com.mapprr.gitsearch.model.Contributor;
import com.mapprr.gitsearch.model.Repo;
import com.mapprr.gitsearch.model.SearchResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by sandy on 29-07-2017.
 */

public interface RestAPI {

    @GET("search/repositories")
    Call<SearchResponse> searchRepository(@Query("q") String q, @Query("sort") String sort, @Query("order") String order, @Query("per_page") int perPage, @Query("page") int page);

    @GET("repos/{login}/{project}/contributors")
    Call<ArrayList<Contributor>> getContributors(@Path("login") String login, @Path("project") String project);

    @GET("users/{login}/repos")
    Call<ArrayList<Repo>> getRepositories(@Path("login") String login);


}

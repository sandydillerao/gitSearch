package com.mapprr.gitsearch;

import android.app.Application;

import com.mapprr.gitsearch.model.Filter;

/**
 * Created by sandy on 29-07-2017.
 */

public class MyApplication extends Application {

    private static MyApplication mInstance;
    public Filter filter;

    public static MyApplication getInstance(){
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }
}

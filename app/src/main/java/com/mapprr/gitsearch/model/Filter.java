package com.mapprr.gitsearch.model;

/**
 * Created by sandy on 07-11-2017.
 */

public class Filter {
    public String queryBy="sandeep";
    public String sortBy = "watcher";
    public String orderBy = "desc";
    public String createdFrom;
    public String createdTo;
    public int pageNo=1;
    public int perPage = 10;
    public String customQuery;

    public String getQueryBy() {
        return queryBy;
    }

    public void setQueryBy(String queryBy) {
        this.queryBy = queryBy;
    }

    public String getCustomQuery() {
        String q;
        if(createdTo!=null){
            q = this.queryBy + " " + "created:" + this.createdFrom + ".." + this.createdTo;
        }else{
            q = this.queryBy;
        }
        return q;
    }

    public void setCustomQuery(String customQuery) {
        this.customQuery = customQuery;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getCreatedFrom() {
        return createdFrom;
    }

    public void setCreatedFrom(String createdFrom) {
        this.createdFrom = createdFrom;
    }

    public String getCreatedTo() {
        return createdTo;
    }

    public void setCreatedTo(String createdTo) {
        this.createdTo = createdTo;
    }

    public void clearFilter(){
        this.sortBy = "watcher";
        this.orderBy = "desc";
        this.createdTo = null;
        this.createdFrom = null;
        this.perPage = 10;
        this.pageNo = 1;
        this.queryBy = "sandeep";
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
